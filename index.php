<?php

require_once 'Telegram.php';
require_once 'user.php';
include 'token.php';

global $TOKEN;
$telegram = new Telegram($TOKEN);

$data = $telegram->getData();
$message = $data['message'];
$chat_id = $message['chat']['id'];
$text = $message['text'];
$firstname = $message['chat']['first_name'] ?? null;

$COURSES = [
    "Umumiy dasturlash teoriyasi",
    "HTML/CSS",
    "Bootstrap",
    "Git/GitLab",
    "PHP",
    "MySQL",
    "Symfony",
    "VueJS",
];
$UDT_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/1-dars-Umumiy-dasturlash-teoriyasi-07-11'>1-dars. Umumiy dasturlash teoriyasi</a>",
    "2-dars" => "<a href='https://telegra.ph/1-dars-Umumiy-dasturlash-teoriyasi-07-11-2'>2-dars. Umumiy dasturlash teoriyasi</a>",
    "3-dars" => "<a href='https://telegra.ph/3-dars-Umumiy-dasturlash-teoriyas-07-11'>3-dars. Umumiy dasturlash teoriyasi</a>",
    "4-dars" => "<a href='https://telegra.ph/4-dars-Umumiy-dasturlash-teoriyas-07-11'>4-dars. Umumiy dasturlash teoriyasi</a>",
    "5-dars" => "<a href='https://telegra.ph/5-dars-Umumiy-dasturlash-teoriyas-07-11'>5-dars. Umumiy dasturlash teoriyasi</a>",
    "6-dars" => "<a href='https://telegra.ph/6-dars-Umumiy-dasturlash-teoriyas-07-11'>6-dars. Umumiy dasturlash teoriyasi</a>",
    "7-dars" => "<a href='https://telegra.ph/7-darsUmumiy-dasturlash-teoriyasi-07-11'>7-dars. Umumiy dasturlash teoriyasi</a>",
];
$HTML_CSS_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/8-dars-HTML-07-11'>1-dars. HTML</a>",
    "2-dars" => "<a href='https://telegra.ph/9-dars-HTML-07-11'>2-dars. HTML</a>",
    "3-dars" => "<a href='https://telegra.ph/10-dars-CSS-07-11'>3-dars. CSS</a>",
    "4-dars" => "<a href='https://telegra.ph/11-dars-CSS-07-17'>4-dars. CSS</a>",
];
$BOOTSTRAP_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/12-dars-Bootstrap-07-17'>1-dars. Bootstrap</a>",
    "2-dars" => "<a href='https://telegra.ph/13-dars-Bootstrap-07-17'>2-dars. Bootstrap</a>",
];
$GIT_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/14-dars-Git-07-17'>1-dars. Git</a>",
    "2-dars" => "<a href='https://telegra.ph/15-dars-GitLab-07-17'>2-dars. GitLab</a>",
];
$PHP_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/1-dars-PHP-10-30'>1-dars. PHP</a>",
    "2-dars" => "<a href='https://telegra.ph/2-dars-PHP-10-30'>2-dars. PHP</a>",
    "3-dars" => "<a href='https://telegra.ph/3-dars-PHP-10-30'>3-dars. PHP</a>",
    "4-dars" => "<a href='https://telegra.ph/4-dars-PHP-10-30'>4-dars. PHP</a>",
];
$MYSQL_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/1-dars-MySQL-10-30'>1-dars. MySQL</a>",
    "2-dars" => "<a href='https://telegra.ph/2-dars-MySQL-10-30'>2-dars. MySQL</a>",
    "3-dars" => "<a href='https://telegra.ph/3-dars-MySQL-10-30'>3-dars. MySQL</a>",
    "4-dars" => "<a href='https://telegra.ph/4-dars-MySQL-10-30'>4-dars. MySQL</a>",
];
$SYMFONY_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/1-dars-Symfony-10-31'>1-dars. Symfony</a>",
    "2-dars" => "<a href='https://telegra.ph/2-dars-Symfony-10-31'>2-dars. Symfony</a>",
    "3-dars" => "<a href='https://telegra.ph/3-dars-Symfony-10-31'>3-dars. Symfony</a>",
    "4-dars" => "<a href='https://telegra.ph/4-dars-Symfony-10-31'>4-dars. Symfony</a>",
    "5-dars" => "<a href='https://telegra.ph/5-dars-Symfony-10-31'>5-dars. Symfony</a>",
    "6-dars" => "<a href='https://telegra.ph/6-dars-Symfony-10-31'>6-dars. Symfony</a>",
    "7-dars" => "<a href='https://telegra.ph/7-dars-Symfony-10-31'>7-dars. Symfony</a>",
    "8-dars" => "<a href='https://telegra.ph/8-dars-Symfony-10-31'>8-dars. Symfony</a>",
    "9-dars" => "<a href='https://telegra.ph/9-dars-Symfony-10-31'>9-dars. Symfony</a>",
    "10-dars" => "<a href='https://telegra.ph/10-dars-Symfony-10-31'>10-dars. Symfony</a>",
    "11-dars" => "<a href='https://telegra.ph/11-dars-Symfony-10-31'>11-dars. Symfony</a>",
    "12-dars" => "<a href='https://telegra.ph/12-dars-Symfony-10-31'>12-dars. Symfony</a>",
    "13-dars" => "<a href='https://telegra.ph/13-dars-Symfony-10-31'>13-dars. Symfony</a>",
    "Symfony cheat-sheet" => "<a href='https://telegra.ph/Symfony-cheat-sheet-10-31'>Symfony cheat-sheet</a>",
];
$VUEJS_LESSONS = [
    "1-dars" => "<a href='https://telegra.ph/1-dars-VueJS-10-31'>1-dars. VueJS</a>",
    "2-dars" => "<a href='https://telegra.ph/2-dars-VueJS-10-31'>2-dars. VueJS</a>",
    "3-dars" => "<a href='https://telegra.ph/3-dars-VueJS-10-31'>3-dars. VueJS</a>",
    "4-dars" => "<a href='https://telegra.ph/4-dars-VueJS-10-31'>4-dars. VueJS</a>",
    "5-dars" => "<a href='https://telegra.ph/5-dars-VueJS-10-31'>5-dars. VueJS</a>",
    "6-dars" => "<a href='https://telegra.ph/6-dars-VueJS-10-31'>6-dars. VueJS</a>",
];

$LESSONS = [
    "udt" => $UDT_LESSONS,
    "html" => $HTML_CSS_LESSONS,
    "bootstrap" => $BOOTSTRAP_LESSONS,
    "git" => $GIT_LESSONS,
    "php" => $PHP_LESSONS,
    "mysql" => $MYSQL_LESSONS,
    "symfony" => $SYMFONY_LESSONS,
    "vuejs" => $VUEJS_LESSONS
];

if ($text == "/start") {
    showMain();
} else {
    switch (getPage($chat_id)) {
        case "main":
            if ($text == "Web Fullstack kursi haqida ma'lumot") {
                showAbout();
            } elseif ($text == "Kurs tartibi") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "course":
            if (in_array($text, $COURSES)) {
                $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $text]);
                selectCourse($text);
            } elseif ($text == "Orqaga") {
                showMain();
            } else {
                chooseButton();
            }
            break;
        case "udt":
            if (array_key_exists($text, $UDT_LESSONS)) {
                showUdt($UDT_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "html":
            if (array_key_exists($text, $HTML_CSS_LESSONS)) {
                showHtmlCss($HTML_CSS_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "bootstrap":
            if (array_key_exists($text, $BOOTSTRAP_LESSONS)) {
                showBootstrap($BOOTSTRAP_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "git":
            if (array_key_exists($text, $GIT_LESSONS)) {
                showGit($GIT_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "php":
            if (array_key_exists($text, $PHP_LESSONS)) {
                showPhp($PHP_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "mysql":
            if (array_key_exists($text, $MYSQL_LESSONS)) {
                showMySql($MYSQL_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "symfony":
            if (array_key_exists($text, $SYMFONY_LESSONS)) {
                showSymfony($SYMFONY_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
        case "vuejs":
            if (array_key_exists($text, $VUEJS_LESSONS)) {
                showVuejs($VUEJS_LESSONS[$text]);
            } elseif ($text == "Bosh menyu") {
                showMain();
            } elseif ($text == "Orqaga") {
                showCourse();
            } else {
                chooseButton();
            }
            break;
    }
}

function showMain()
{
    global $telegram, $chat_id, $firstname;

    setPage($chat_id, 'main');
    setUserFirstname($chat_id, $firstname);

    $option = array(
        array($telegram->buildKeyboardButton("Web Fullstack kursi haqida ma'lumot")),
        array($telegram->buildKeyboardButton("Kurs tartibi")),
    );
    $keyb = $telegram->buildKeyBoard($option, true, true, true);
    $content = array('chat_id' => $chat_id, 'text' => "Assalomu aleykum $firstname, botimizga xush kelibsiz!");
    $telegram->sendMessage($content);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => "Bu bot sizlarga Akmal Kadirov ning Web Fullstack development kursini o'rgatadi!"
    );
    $telegram->sendMessage($content);
}

function showAbout()
{
    global $telegram, $chat_id;

    $option = array(
        array($telegram->buildKeyboardButton("Web Fullstack kursi haqida ma'lumot")),
        array($telegram->buildKeyboardButton("Kurs tartibi")),
    );
    $keyb = $telegram->buildKeyBoard($option, true, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => "<a href='https://telegra.ph/Web-Full-Stack-developer-Kursi-07-11'>Web Fullstack kursi haqida ma'lumot</a>",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showCourse()
{
    global $telegram, $chat_id;

    setPage($chat_id, 'course');

    $option = array(
        array(
            $telegram->buildKeyboardButton("Umumiy dasturlash teoriyasi"),
            $telegram->buildKeyboardButton("HTML/CSS")
        ),
        array($telegram->buildKeyboardButton("Bootstrap"), $telegram->buildKeyboardButton("Git/GitLab")),
        array($telegram->buildKeyboardButton("PHP"), $telegram->buildKeyboardButton("MySQL")),
        array($telegram->buildKeyboardButton("Symfony"), $telegram->buildKeyboardButton("VueJS")),
        array($telegram->buildKeyboardButton("Orqaga"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇"
    );
    $telegram->sendMessage($content);
}

function chooseButton()
{
    global $telegram, $chat_id;

    $content = array(
        'chat_id' => $chat_id,
        'text' => "👇Iltimos quyidagi tugmalardan birini tanlang!👇"
    );
    $telegram->sendMessage($content);
}

function showUdt($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'udt');

    $option = array(
        array(
            $telegram->buildKeyboardButton("1-dars"),
            $telegram->buildKeyboardButton("2-dars")
        ),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("5-dars"), $telegram->buildKeyboardButton("6-dars")),
        array($telegram->buildKeyboardButton("7-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showHtmlCss($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'html');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showBootstrap($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'bootstrap');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showGit($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'git');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showPhp($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'php');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showMySql($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'mysql');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showSymfony($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'symfony');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("5-dars"), $telegram->buildKeyboardButton("6-dars")),
        array($telegram->buildKeyboardButton("7-dars"), $telegram->buildKeyboardButton("8-dars")),
        array($telegram->buildKeyboardButton("9-dars"), $telegram->buildKeyboardButton("10-dars")),
        array($telegram->buildKeyboardButton("11-dars"), $telegram->buildKeyboardButton("12-dars")),
        array($telegram->buildKeyboardButton("13-dars"), $telegram->buildKeyboardButton("Symfony cheat-sheet")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function showVuejs($data = null)
{
    global $telegram, $chat_id;

    setPage($chat_id, 'vuejs');

    $option = array(
        array($telegram->buildKeyboardButton("1-dars"), $telegram->buildKeyboardButton("2-dars")),
        array($telegram->buildKeyboardButton("3-dars"), $telegram->buildKeyboardButton("4-dars")),
        array($telegram->buildKeyboardButton("5-dars"), $telegram->buildKeyboardButton("6-dars")),
        array($telegram->buildKeyboardButton("Orqaga"), $telegram->buildKeyboardButton("Bosh menyu"))
    );
    $keyb = $telegram->buildKeyBoard($option, true, true);
    $content = array(
        'chat_id' => $chat_id,
        'reply_markup' => $keyb,
        'text' => $data ?? "👇Darsni boshlash uchun quyidagi tugmalardan birini tanlang!👇",
        'parse_mode' => 'html'
    );
    $telegram->sendMessage($content);
}

function selectCourse($text)
{
    if ($text == "Umumiy dasturlash teoriyasi") {
        showUdt();
    } elseif ($text == "HTML/CSS") {
        showHtmlCss();
    } elseif ($text == "Bootstrap") {
        showBootstrap();
    } elseif ($text == "Git/GitLab") {
        showGit();
    } elseif ($text == "PHP") {
        showPhp();
    } elseif ($text == "MySQL") {
        showMySql();
    } elseif ($text == "Symfony") {
        showSymfony();
    } elseif ($text == "VueJS") {
        showVuejs();
    }
}

function sendMessageJson()
{
    global $telegram, $data;

    $telegram->sendMessage([
        'chat_id' => $telegram->ChatID(),
        'text' => json_encode($data, JSON_PRETTY_PRINT)
    ]);
}
