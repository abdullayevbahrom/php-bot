<?php

function setPage($chat_id, $data)
{
    file_put_contents('users/' . $chat_id . 'page.txt', $data);
}

function getPage($chat_id)
{
    return file_get_contents('users/' . $chat_id . 'page.txt');
}

function setUserFirstname($chat_id, $data)
{
    file_put_contents('users/' . $chat_id . 'user.txt', $data);
}

function getUserFirstname($chat_id)
{
    return file_get_contents('users/' . $chat_id . 'user.txt');
}